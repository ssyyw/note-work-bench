# git命令使用学习
## 文档
http://git.oschina.net/progit/
https://git-scm.com/docs
## 分支处理

## 全局git设置

```bash
$ git config --global user.name "你的名字或昵称"
$ git config --global user.email "你的邮箱"
```

## 提交、推送、拉取等

```bash
$ git add . #将当前目录所有文件添加到git暂存区
$ git commit -m "my first commit" #提交并备注提交信息
$ git push origin master #将本地提交推送到远程仓库
$ git init
#连接远程仓库
$ git remote add origin https://gitee.com/用户个性地址/HelloGitee.git
$ git pull origin master
#全文件添加
$ git add .
$ git commit -m "第一次提交"
$ git push origin master
#强制推送
$ git push origin master -f
#保留文件推送
$ git pull origin master
git config --global user.name "ASxx"
git config --global user.email "123456789@qq.com"  
git config --global credential.helper store    
```

## 




```bash
#强制同步
git pull origin master --allow-unrelated-histories
#拉取、推送
git fetch [remote-name]
#创建一个新分支
git branch branch1
#检测分支
git branch
#状态检测
git status
# 切换分支
git checkout branch
#合并分支
git merge branch1
#删除分支
git branch -D newbranch
#提交
git push -u origin master
```

在合并项目分支的时候，比如将我自己的wyl分支合并到dev分支上，采用如下步骤：

git status查看本地分支状态，需要将待合并分支和被合并分支都拉取到本地。比如现在我处于wyl本地分支上，输入命令git checkout dev切换到dev分支，并git pull将dev分支拉取到本地。
并输入命令yarn build，或者npm run build，将项目进行打包，如果打包过程中出现错误，比如：双向绑定没有对应数据，组件名重复等问题，按照终端的提示进行修改，然后重新输入yarn build，直到成功打包为止。成功打包后，会在项目文件的一级目录里出现dist文件，dist文件让我们我们就可以像打开静态网页一样打开我们完成的项目。最后将dist文件删除即可。
输入命令git merge wyl，将wyl分支合并到本地dev分支。如果出现冲突，左侧导航栏的对应模块项会变色，或者通过终端里的提示，找到相应冲突并解决。
解决完所有冲突后，再一次重复步骤2，将项目打包检查并解决冲突。
按照上文所说的推送分支的其他方法进行推送，这样就能成功合并。
PS：在本地没有解决完冲突一定不能推送。
​	