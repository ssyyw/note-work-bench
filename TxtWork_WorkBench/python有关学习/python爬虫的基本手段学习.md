# python爬虫的学习笔记
## 总览
总的而言，与python的各种有关的应用都是基于各种库，各种框架的。爬虫也是这样，一般情况下，爬虫会用到的库包括
1. requests
2. urllib
3. BeautifulSoup
4. ...

> 不管爬取什么网站都应该尽量遵守网站的robots协议（当前爬虫访问速度和人类接近时可以不遵守）

## requests库
### 简介
requests库在爬虫中一般都是用于网页资源的获取，使用非常简洁。

使用pip install requests安装

### 主要内容

> 在requests库中定义了一个response对象，用于返回获取的内容

比如在shell中输入如下内容：

```python
import requests
response = requests.get("http://www.baidu.com")
r.status_code
r.text[:1000]
```

会得到：
![]()
1. request.get() 得到对应的response对象，用于后续操作
2. status_code 得到访问的状态码，除了200，全都是不正常的
3. text 以txt输出得到的内容




## 正则

> 正则表达式的编译：将符合正则表达式语法的字符串转换成正则表达式特征
#### 